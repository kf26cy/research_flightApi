package controllers;

import play.Logger;
import play.libs.F;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import services.person.UserService;

import javax.inject.Inject;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
public class UsersResource extends Controller {

    @Inject
    private UserService userService;


    public F.Promise<Result> getUser() {
        F.Promise<WSResponse> response = userService.getUser();
        return response.map(wsResponse -> {
            return wsResponse.getBody().startsWith("<") ? ok(wsResponse.asByteArray())  : ok(wsResponse.asJson());
        });
    }

}
