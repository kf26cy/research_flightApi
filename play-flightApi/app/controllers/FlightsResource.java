package controllers;

import com.google.inject.Inject;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.flights.FlightService;
import services.flights.SearchFlightRequest;
import services.flights.result.FlightResult;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
public class FlightsResource extends Controller {

    @Inject
    private FlightService flightService;


    @Transactional
    public Result searchFlights(String start,
                                String end,

                                String source,
                                String destination,

                                String oneway,
                                String direct,

                                String min,
                                String max) {

        List<FlightResult> flightResults = flightService.processSearchRequest(SearchFlightRequest.SearchFlightRequestBuilder.aSearchFlightRequest()
                .withStart(Long.parseLong(start))
                .withEnd(Long.parseLong(end))
                .withSourceAirportId(Long.parseLong(source))
                .withDestinationAirportId(Long.parseLong(destination))
                .withOneWay(Boolean.valueOf(oneway))
                .withDirectFlight(Boolean.valueOf(direct))
                .withMinPrice(new BigDecimal(min))
                .withMaxPrice(max != null ? new BigDecimal(max) : null)
                .build());

        return ok(Json.toJson(flightResults));
    }


}
