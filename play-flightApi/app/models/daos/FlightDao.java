package models.daos;

import models.entities.Flight;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import util.DateUtils;

import javax.inject.Singleton;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
@Singleton
public class FlightDao {


    public List<Flight> betweenABon(Long sAirportId, Long dAirportId, Date departure) {
        Date bod = DateUtils.goToFirstHour(departure);
        Date eod = DateUtils.goToEndOfTheDay(departure);

        Query query = JPA.em().createQuery("SELECT f " +
                "FROM Flight f " +
                "WHERE f.sourceAirport.id = :sAirport " +
                "AND f.destinationAirport.id = :dAirport " +
                "AND f.departure > :bod AND f.departure < :eod");

        query.setParameter("sAirport", sAirportId).setParameter("dAirport", dAirportId)
                .setParameter("bod", bod).setParameter("eod", eod);

        List<Flight> resultList = query.getResultList();
        return resultList != null ? resultList : new ArrayList<Flight>();
    }


    public List<Flight> leavingAon(Long sAirportId, Date departure) {
        Date bod = DateUtils.goToFirstHour(departure);
        Date eod = DateUtils.goToEndOfTheDay(departure);

        Query query = JPA.em().createQuery("SELECT f " +
                "FROM Flight f " +
                "WHERE f.sourceAirport.id = :sAirport " +
                "AND f.departure > :bod AND f.departure < :eod");

        query.setParameter("sAirport", sAirportId).setParameter("bod", bod).setParameter("eod", eod);

        List<Flight> results = query.getResultList();
        return results != null ? results : new ArrayList<Flight>();
    }


    public List<Flight> arrivingInBon(Long dAirportId, Date arrival) {
        Date bod = DateUtils.goToFirstHour(arrival);
        Date eod = DateUtils.goToEndOfTheDay(arrival);

        Query query = JPA.em().createQuery("SELECT f " +
                "FROM Flight f " +
                "WHERE f.destinationAirport.id = :dAirport " +
                "AND f.departure > :bod AND f.departure < :eod");

        query.setParameter("dAirport", dAirportId).setParameter("bod", bod).setParameter("eod", eod);

        List<Flight> results = query.getResultList();
        return results != null ? results : new ArrayList<Flight>();
    }


}
