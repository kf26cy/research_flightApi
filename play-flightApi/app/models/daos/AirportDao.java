package models.daos;

import models.entities.Airport;
import models.entities.Person;
import play.db.jpa.JPA;

import javax.inject.Singleton;
import javax.persistence.Query;
import java.util.List;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
@Singleton
public class AirportDao {

    public List<Airport> loadAll() {
        Query query = JPA.em().createQuery("SELECT a FROM Airport a");
        return query.getResultList();
    }


    public Airport findById(Long id) {
        return JPA.em().find(Airport.class, id);
    }
}
