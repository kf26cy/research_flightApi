package models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 25/May/2014
 */

@Entity
@Table(name = "FLIGHTS")
public class Flight extends AbstractEntity {

    @Column(name = "departure", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date departure;

    @Column(name = "arrival", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dueArrival;

    @ManyToOne
    @JoinColumn(name = "source_airport_id", nullable = false)
    private Airport sourceAirport;

    @ManyToOne
    @JoinColumn(name = "dest_airport_id", nullable = false)
    private Airport destinationAirport;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

    @Column(name = "company")
    private String company;



    public Flight() {}

    public Flight(Date departure, Date dueArrival, Airport sourceAirport, Airport destinationAirport, BigDecimal price, String company) {
        this.departure = departure;
        this.dueArrival = dueArrival;
        this.sourceAirport = sourceAirport;
        this.destinationAirport = destinationAirport;
        this.price = price;
        this.company = company;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getDueArrival() {
        return dueArrival;
    }

    public void setDueArrival(Date dueArrival) {
        this.dueArrival = dueArrival;
    }

    public Airport getSourceAirport() {
        return sourceAirport;
    }

    public void setSourceAirport(Airport sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public Airport getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(Airport destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
