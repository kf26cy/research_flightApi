package models.entities;

import javax.persistence.*;

//@Entity
//@Table(name = "PERSONS")
public class Person {

    @Id
    @GeneratedValue
    private Long personId;

    @Column(name = "username")
    private String name;

    @Column(name = "pass")
    private String pass;


    public Person() {
    }

    public Person(String name, String pass) {
        this.name = name;
        this.pass = pass;
    }


    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
