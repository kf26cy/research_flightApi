package services.flights;

import com.google.inject.Inject;
import models.daos.AirportDao;
import models.entities.Airport;

import javax.inject.Singleton;
import java.util.List;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Singleton
public class AirportService {

    @Inject
    private AirportDao airportDao;



    public List<Airport> loadAll() {
        return airportDao.loadAll();
    }



    public Airport findAirportById(Long id) {
        if (id == null) {
            return null;
        }
        return airportDao.findById(id);

    }
}
