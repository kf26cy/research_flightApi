package services.flights.result;

import java.math.BigDecimal;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
public class FlightResult {

    private Trip forward;
    private Trip back;
    private BigDecimal price;

    public FlightResult(Trip forward) {
        this(forward, null);
    }

    public FlightResult(Trip forward, Trip back) {
        this.forward = forward;
        this.back = back;
        computePrice();
    }

    private void computePrice() {
        BigDecimal initialAmount = this.forward.getPrice().add(this.back != null ? this.back.getPrice() : new BigDecimal(0));
        this.price = initialAmount;
    }

    public Trip getForward() {
        return forward;
    }

    public Trip getBack() {
        return back;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
