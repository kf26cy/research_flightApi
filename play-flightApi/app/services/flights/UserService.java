package services.flights;


import services.person.RandomApiResponse;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
public interface UserService {


    public RandomApiResponse getUser();


}
