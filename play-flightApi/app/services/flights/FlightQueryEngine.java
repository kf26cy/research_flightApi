package services.flights;

import models.daos.FlightDao;
import models.entities.Flight;
import play.db.jpa.Transactional;
import services.flights.result.Trip;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Singleton
public class FlightQueryEngine {

    @Inject
    private FlightDao flightDao;


    public List<Trip> findDirectOneWayTrips (Long sourceAirportId, Long destinationAirportId, Date start) {
        List<Flight> flights = flightDao.betweenABon(sourceAirportId, destinationAirportId, start);
        return toTrips(flights);
    }


    public List<Trip> findAllOneWayTrips(Long sourceAirportId, Long destinationAirportId, Date start) {
        List<Trip> directTrips = findDirectOneWayTrips(sourceAirportId, destinationAirportId, start);

        List<Flight> leavingFrom = flightDao.leavingAon(sourceAirportId, start);
        List<Flight> arrivingAt = flightDao.arrivingInBon(destinationAirportId, start);

        List<Trip> indirectTrips = toTrips(leavingFrom, arrivingAt);

        directTrips.addAll(indirectTrips);
        return directTrips;
    }



    private List<Trip> toTrips(List<Flight> step1Flights) {
        List<Trip> trips = new ArrayList<Trip>();

        for (Flight flight : step1Flights) {
            trips.add(new Trip(flight));
        }
        return trips;
    }


    private List<Trip> toTrips(List<Flight> leavingFrom, List<Flight> arrivingAt) {
        List<Trip> trips = new ArrayList<Trip>();

        for (Flight leaving : leavingFrom) {
            for (Flight arriving : arrivingAt) {
                if (!leaving.getDestinationAirport().equals(arriving.getSourceAirport())) {
                    continue;
                }
                trips.add(new Trip(leaving, arriving));
            }
        }
        return trips;
    }
}
