package services.flights;


import util.DateUtils;

import java.util.Date;
import java.util.concurrent.TimeUnit;




public class DiscountCalculator {

    private static final int MONTH_DISCOUNT_UNIT = 2;
    private static final int MONTH_DICOUNT_DIVISION_FACTOR = 5;

    public static long getDiscount(Date departure) {
        long daysAppart = DateUtils.getDateDiff(departure, new Date(), TimeUnit.DAYS) / 30;
        long monthsApart = daysAppart / 30;

        if (monthsApart == 0) {
            return 200 - (daysAppart * 4) % 40;
        }

        long monthDiscount = (2 ^ monthsApart) % 30 + MONTH_DISCOUNT_UNIT * monthsApart;
        return 150 - monthDiscount + getDayInWeekDiscount(DateUtils.getDayInTheWeek(departure));
    }

    private static long getDayInWeekDiscount(int day) {
        switch(day) {
            case 0: return 4;
            case 1: return 5;
            case 2: return 5;
            case 3: return 2;
            case 4: return 1;
            case 5: return 0;
            case 6: return 0;
            default: return 0;
        }
    }

}
