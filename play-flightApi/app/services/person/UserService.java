package services.person;

import com.google.inject.Inject;
import play.libs.ws.WSClient;
import play.libs.F;
import play.libs.ws.WSResponse;

import javax.inject.Singleton;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
@Singleton
public class UserService {

    @Inject
    private WSClient wsclient;

    private static final String API_URL = "https://randomuser.me/api";


    public F.Promise<WSResponse> getUser() {
        return wsclient.url(API_URL).get();
    }

}
