package services.person;

import java.util.List;

/**
 * Created by ktaDinca on 9/8/2015.
 */
public class RandomApiResponse {

    List<Result> results;

    public List<Result> 	getResults		() 						{ return results; }
    public void 			setResults		(List<Result> results) 	{ this.results = results; }

    public static class Result {
        private User user;
        private String seed;

        public User getUser     ()              { return user; }
        public void setUser     (User user)     { this.user = user; }
        public String getSeed   ()              { return seed; }
        public void setSeed     (String seed)   { this.seed = seed; }


        public static class User {
            private String gender;
            private Name name;
            private Location location;
            private String email;
            private String username;
            private String password;
            private String salt;
            private String md5;
            private String sha1;
            private String sha256;
            private String registered;
            private String dob;
            private String phone;
            private String cell;
            private String DNI;
            private Picture picture;
            private String version;
            private String nationality;


            public String      getGender       ()                  { return gender; }
            public void        setGender       (String gender)     { this.gender = gender; }
            public Name        getName         ()                  { return name; }
            public void        setName         (Name name)         { this.name = name; }
            public Location    getLocation     ()                  { return location; }
            public void        setLocation     (Location location) { this.location = location; }
            public String      getEmail        ()                  { return email; }
            public void        setEmail        (String email)      { this.email = email; }
            public String      getUsername     ()                  { return username; }
            public void        setUsername     (String username)   { this.username = username; }
            public String      getPassword     ()                  { return password; }
            public void        setPassword     (String password)   { this.password = password; }
            public String      getSalt         ()                  { return salt; }
            public void        setSalt         (String salt)       { this.salt = salt; }
            public String      getMd5          ()                  { return md5; }
            public void        setMd5          (String md5)        { this.md5 = md5; }
            public String      getSha1         ()                  { return sha1; }
            public void        setSha1         (String sha1)       { this.sha1 = sha1; }
            public String      getSha256       ()                  { return sha256; }
            public void        setSha256       (String sha256)     { this.sha256 = sha256; }
            public String      getRegistered   ()                  { return registered; }
            public void        setRegistered   (String registered) { this.registered = registered; }
            public String      getDob          ()                  { return dob; }
            public void        setDob          (String dob)        { this.dob = dob; }
            public String      getPhone        ()                  { return phone; }
            public void        setPhone        (String phone)      { this.phone = phone; }
            public String      getCell         ()                  { return cell; }
            public void        setCell         (String cell)       { this.cell = cell; }
            public String      getDNI          ()                  { return DNI; }
            public void        setDNI          (String DNI)        { this.DNI = DNI; }
            public Picture     getPicture      ()                  { return picture; }
            public void        setPicture      (Picture picture)   { this.picture = picture; }
            public String      getVersion      ()                  { return version; }
            public void        setVersion      (String version)    { this.version = version; }
            public String      getNationality  ()                  { return nationality; }
            public void        setNationality  (String nationality){ this.nationality = nationality; }

            private static class Location {
                private String street;
                private String city;
                private String state;
                private String zip;

                public String getStreet     ()                  {return street; }
                public void setStreet       (String street)     {this.street = street;}
                public String getCity       ()                  { return city; }
                public void setCity(        String city)        { this.city = city; }
                public String getState      ()                  { return state; }
                public void setState        (String state)      { this.state = state; }
                public String getZip        ()                  {return zip;}
                public void setZip          (String zip)        { this.zip = zip;}
            }

            private static class Name {
                private String title;
                private String first;
                private String last;

                public String getTitle      ()                  { return title; }
                public void setTitle        (String title)      { this.title = title; }
                public String getFirst      ()                  { return first; }
                public void setFirst        (String first)      { this.first = first; }
                public String getLast       ()                  { return last; }
                public void setLast         (String last)       { this.last = last; }
            }

            private static class Picture {
                private String large;
                private String medium;
                private String thumbnail;

                public String getLarge      ()                  { return large; }
                public void setLarge        (String large)      { this.large = large; }
                public String getMedium     ()                  { return medium; }
                public void setMedium(      String medium)      { this.medium = medium; }
                public String getThumbnail  ()                  { return thumbnail; }
                public void setThumbnail    (String thumbnail)  { this.thumbnail = thumbnail; }
            }
        }
    }
}
