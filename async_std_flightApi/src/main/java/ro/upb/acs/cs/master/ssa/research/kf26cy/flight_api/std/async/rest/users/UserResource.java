package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.rest.users;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.rest.util.ResponseUtils;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.impl.UserService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
@Component
@Path("/user")
@Consumes("application/json")
@Produces("application/json")
public class UserResource {

    @Autowired
    private UserService userService;


    @GET
    public void getRandomUser(@Suspend(10000) AsynchronousResponse response) {
        Runnable worker = () -> {
            response.setResponse(ResponseUtils.createSuccessfulResponse(userService.getUser()));
        };
        new Thread(worker).start();
    }

}
