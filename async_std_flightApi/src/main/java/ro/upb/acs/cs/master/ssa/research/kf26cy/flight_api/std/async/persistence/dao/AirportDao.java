package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.dao;

import org.springframework.stereotype.Repository;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.model.Airport;

import javax.persistence.Query;
import java.util.List;


/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Repository
public class AirportDao extends BaseDao {

    public List<Airport> loadAll() {
        Query q = entityManager.createQuery("Select a from Airport a order by a.name");
        return q.getResultList();
    }


    public Airport findAirportById(Long id) {
        Query q = entityManager.createQuery("Select a from Airport a where a.id = :id");
        q.setParameter("id", id);

        List<Airport> results = q.getResultList();
        if (results != null && results.size() > 0) {
            return results.get(0);
        }
        return null;
    }
}
