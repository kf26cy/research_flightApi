package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.dao.AirportDao;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.dao.FlightDao;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.model.Airport;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.model.Flight;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.util.DateUtils;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/09/15.
 */

@Service
@Transactional
public class FlightsGenerator {


    private SecureRandom random = new SecureRandom();
    private static final int SAMPLING_PERIOD = 365;

    @Autowired
    private AirportDao airportDao;

    @Autowired
    private FlightDao flightDao;


    private List<String> airCompanies = Arrays.asList("TAROM", "AIR FRANCE", "KLM", "BRITISH AIRWAYS", "AL ITALIA", "LUFTHANSA");


    public void generateFlights() {
        setupAirports();
        List<Airport> airports = airportDao.loadAll();

        for (Airport source: airports) {
            for (Airport destination : airports) {
                if (source.equals(destination)) {
                    continue;
                }
                if (random.nextBoolean()) {
                    buildFlightPlan(source, destination);
                }
            }
        }
    }


    private void buildFlightPlan(Airport source, Airport destination) {
        for (String company : airCompanies) {
            if (!random.nextBoolean()) {
                continue;
            }

            Date _departure = DateUtils.goToSpecificTime(new Date(), random.nextInt(12), random.nextInt(60));
            Date _arrival = DateUtils.addXMinutesToDate(_departure, random.nextInt(300));

            for (int i = 0; i < SAMPLING_PERIOD; i ++) {
                Flight flight = new Flight(_departure, _arrival, source, destination, new BigDecimal(random.nextInt(500)), company);
                flightDao.saveOrUpdate(flight);

                _departure = DateUtils.nextDay(_departure);
                _arrival = DateUtils.nextDay(_arrival);
            }
        }
    }


    private void setupAirports() {
        List<String> _airports = Arrays.asList("Bucharest", "London",
                "Paris", "Madrid", "Amsterdam",
                "Oslo", "Berlin", "Barcelona", "Rome",
                "Milan", "Kiev", "Prague", "Brussels",
                "Marseille", "Munich", "Venice");

        for (String $airport : _airports) {
            Airport airport = new Airport(buildAirportCode($airport), $airport, getRandomCoordinate(), getRandomCoordinate());
            airportDao.saveOrUpdate(airport);
        }
    }


    private String buildAirportCode(String $airport) {
        return $airport.substring(0, 3).toUpperCase();
    }


    private Float getRandomCoordinate() {
        return new Float(Math.random());
    }

}
