package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service;

import java.math.BigDecimal;
import java.util.Date;


public class SearchFlightRequest {

    private Date start;
    private Date end;

    private Long sourceAirportId;
    private Long destinationAirportId;

    private BigDecimal minPrice;
    private BigDecimal maxPrice;

    private boolean oneWay;
    private boolean directFlight;


    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Long getSourceAirportId() {
        return sourceAirportId;
    }

    public void setSourceAirportId(Long sourceAirportId) {
        this.sourceAirportId = sourceAirportId;
    }

    public Long getDestinationAirportId() {
        return destinationAirportId;
    }

    public void setDestinationAirportId(Long destinationAirportId) {
        this.destinationAirportId = destinationAirportId;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public boolean isOneWay() {
        return oneWay;
    }

    public void setOneWay(boolean oneWay) {
        this.oneWay = oneWay;
    }

    public boolean isDirectFlight() {
        return directFlight;
    }

    public void setDirectFlight(boolean directFlight) {
        this.directFlight = directFlight;
    }


    public static class SearchFlightRequestBuilder {

        private Long start;
        private Long end;
        private Long sourceAirportId;
        private Long destinationAirportId;
        private BigDecimal minPrice;
        private BigDecimal maxPrice;
        private boolean oneWay;
        private boolean directFlight;

        private SearchFlightRequestBuilder() {
        }

        public static SearchFlightRequestBuilder aSearchFlightRequest() {
            return new SearchFlightRequestBuilder();
        }

        public SearchFlightRequestBuilder withStart(Long start) {
            this.start = start;
            return this;
        }

        public SearchFlightRequestBuilder withEnd(Long end) {
            this.end = end;
            return this;
        }

        public SearchFlightRequestBuilder withSourceAirportId(Long sourceAirportId) {
            this.sourceAirportId = sourceAirportId;
            return this;
        }

        public SearchFlightRequestBuilder withDestinationAirportId(Long destinationAirportId) {
            this.destinationAirportId = destinationAirportId;
            return this;
        }

        public SearchFlightRequestBuilder withMinPrice(BigDecimal minPrice) {
            this.minPrice = minPrice;
            return this;
        }

        public SearchFlightRequestBuilder withMaxPrice(BigDecimal maxPrice) {
            this.maxPrice = maxPrice;
            return this;
        }

        public SearchFlightRequestBuilder withOneWay(boolean oneWay) {
            this.oneWay = oneWay;
            return this;
        }

        public SearchFlightRequestBuilder withDirectFlight(boolean directFlight) {
            this.directFlight = directFlight;
            return this;
        }

        public SearchFlightRequestBuilder but() {
            return aSearchFlightRequest().withStart(start).withEnd(end).withSourceAirportId(sourceAirportId).withDestinationAirportId(destinationAirportId).withMinPrice(minPrice).withMaxPrice(maxPrice).withOneWay(oneWay).withDirectFlight(directFlight);
        }

        public SearchFlightRequest build() {
            SearchFlightRequest searchFlightRequest = new SearchFlightRequest();
            searchFlightRequest.setStart(new Date(start));
            searchFlightRequest.setEnd(new Date(end));
            searchFlightRequest.setSourceAirportId(sourceAirportId);
            searchFlightRequest.setDestinationAirportId(destinationAirportId);
            searchFlightRequest.setMinPrice(minPrice);
            searchFlightRequest.setMaxPrice(maxPrice);
            searchFlightRequest.setOneWay(oneWay);
            searchFlightRequest.setDirectFlight(directFlight);
            return searchFlightRequest;
        }
    }
}
