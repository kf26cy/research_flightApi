package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.impl;

import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.person.RandomApiResponse;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
public interface UserService {


    public RandomApiResponse getUser();


}
