package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.SearchFlightRequest;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.Trip;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.FlightResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Service
public class FlightService {

    @Autowired
    private FlightQueryEngine flightQueryEngine;


    public List<FlightResult> processSearchRequest(SearchFlightRequest request) {

        List<FlightResult> results = new ArrayList<FlightResult>();

        int isOneWay = request.isOneWay() ? 1 : -1;
        int isDirect = request.isDirectFlight() ? 2 : 4;
        int flightConstraints = isOneWay * isDirect;

        switch(flightConstraints) {
            /* one way direct flights */
            case 2 :
                results = getDirectOneWayFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart());
                break;

            /* one way indirect flight */
            case 4 :
                results = getIndirectOneWayFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart());
                break;

            /* round trip direct flight*/
            case -2 :
                results = getDirectRoundTripFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart(), request.getEnd());
                break;

            /* round trip indirect flight*/
            case -4 :
                results = getIndirectRoundTripFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart(), request.getEnd());
                break;
        }

        return filterResulsByPriceConstraints(results, request.getMinPrice(), request.getMaxPrice());
    }



    private List<FlightResult> filterResulsByPriceConstraints(List<FlightResult> results, final BigDecimal min, final BigDecimal max) {

        CollectionUtils.filter(results, new Predicate<FlightResult>() {

            @Override
            public boolean evaluate(FlightResult flightResult) {
                if (max == null) {
                    return flightResult.getPrice().compareTo(min) > 0;
                }
                else {
                    return flightResult.getPrice().compareTo(min) > 0 && flightResult.getPrice().compareTo(max) < 0;
                }
            }
        });

        return results;
    }



    private List<FlightResult> getIndirectRoundTripFlights(Long sourceAirportId, Long destinationAirportId, Date start, Date end) {
        List<Trip> forward = flightQueryEngine.findAllOneWayTrips(sourceAirportId, destinationAirportId, start);
        List<Trip> back = flightQueryEngine.findAllOneWayTrips(destinationAirportId, sourceAirportId, end);

        return buildResults(forward, back);
    }


    private List<FlightResult> getDirectRoundTripFlights(Long sourceAirportId, Long destinationAirportId, Date start, Date end) {
        List<Trip> forward = flightQueryEngine.findDirectOneWayTrips(sourceAirportId, destinationAirportId, start);
        List<Trip> back = flightQueryEngine.findDirectOneWayTrips(destinationAirportId, sourceAirportId, end);

        return buildResults(forward, back);
    }


    private List<FlightResult> getIndirectOneWayFlights(Long sourceAirportId, Long destinationAirportId, Date start) {
        return buildResults(flightQueryEngine.findAllOneWayTrips(sourceAirportId, destinationAirportId, start));
    }


    private List<FlightResult> getDirectOneWayFlights(Long sourceAirportId, Long destinationAirportId, Date start) {
        return buildResults(flightQueryEngine.findDirectOneWayTrips(sourceAirportId, destinationAirportId, start));
    }


    private List<FlightResult> buildResults(List<Trip> oneWayTrips) {
        if (oneWayTrips == null) {
            return new ArrayList<FlightResult>();
        }

        List<FlightResult> results = new ArrayList<FlightResult>();
        for(Trip trip : oneWayTrips) {
            results.add(new FlightResult(trip));
        }
        return results;
    }


    private List<FlightResult> buildResults(List<Trip> forward, List<Trip> back) {
        if (forward == null || back == null) {
            return new ArrayList<FlightResult>();
        }
        ArrayList<FlightResult> results = new ArrayList<FlightResult>();

        for(Trip _forward : forward) {
            for(Trip _back : back) {
                results.add(new FlightResult(_forward, _back));
            }
        }
        return results;
    }

}
