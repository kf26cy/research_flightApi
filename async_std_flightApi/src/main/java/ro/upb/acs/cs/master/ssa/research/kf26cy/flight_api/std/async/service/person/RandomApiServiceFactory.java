package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.person;

import com.squareup.okhttp.OkHttpClient;
import org.springframework.stereotype.Service;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by ktaDinca on 9/8/2015.
 */
@Service
public class RandomApiServiceFactory {

    private static final String API_URL = "https://randomuser.me/";


    public RandomApiService createService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        return restAdapter.create(RandomApiService.class);
    }


}
