package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.impl.UserService;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
@Service
public class UserServiceRandomApiImpl implements UserService {

    @Autowired
    private RandomApiServiceFactory factory;


    @Override
    public RandomApiResponse getUser() {
        return factory.createService().getRandomUser();
    }

}
