package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.dao.AirportDao;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.model.Airport;

import java.util.List;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Service
public class AirportService {

    @Autowired
    private AirportDao airportDao;


    public List<Airport> loadAll() {
        return airportDao.loadAll();
    }

    public Airport findAirportById(Long id) {
        if (id == null) {
            return null;
        }
        return airportDao.findAirportById(id);

    }
}
