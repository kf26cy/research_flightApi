package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service;

import java.math.BigDecimal;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
public class FlightResult {

    private Trip forward;
    private Trip back;
    private BigDecimal price;

    public FlightResult(Trip forward) {
        this(forward, null);
    }

    public FlightResult(Trip forward, Trip back) {
        this.forward = forward;
        this.back = back;
        computePrice();
    }

    private void computePrice() {
        BigDecimal initialAmount = this.forward.getPrice().add(this.back != null ? this.back.getPrice() : new BigDecimal(0));
//        long discountPercentage = DiscountCalculator.getDiscount(this.getForward().getStep1().getDeparture());
//        this.price =  initialAmount.multiply(new BigDecimal(discountPercentage)).divide(new BigDecimal(100));
        this.price = initialAmount;
    }

    public Trip getForward() {
        return forward;
    }

    public Trip getBack() {
        return back;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
