package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.dao;

import org.springframework.stereotype.Repository;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.model.Flight;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.util.DateUtils;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Repository
public class FlightDao extends BaseDao {


    public List<Flight> betweenABon(Long sAirportId, Long dAirportId, Date departure) {
        Date bod = DateUtils.goToFirstHour(departure);
        Date eod = DateUtils.goToEndOfTheDay(departure);

        Query query = entityManager.createQuery("SELECT f " +
                "FROM Flight f " +
                "WHERE f.sourceAirport.id = :sAirport " +
                "AND f.destinationAirport.id = :dAirport " +
                "AND f.departure > :bod AND f.departure < :eod");

        query.setParameter("sAirport", sAirportId).setParameter("dAirport", dAirportId)
                .setParameter("bod", bod).setParameter("eod", eod);

        List<Flight> resultList = query.getResultList();
        return resultList != null ? resultList : new ArrayList<Flight>();
    }



    public List<Flight> leavingAon(Long sAirportId, Date departure) {
        Date bod = DateUtils.goToFirstHour(departure);
        Date eod = DateUtils.goToEndOfTheDay(departure);

        Query query = entityManager.createQuery("SELECT f " +
                "FROM Flight f " +
                "WHERE f.sourceAirport.id = :sAirport " +
                "AND f.departure > :bod AND f.departure < :eod");

        query.setParameter("sAirport", sAirportId).setParameter("bod", bod).setParameter("eod", eod);

        List<Flight> results = query.getResultList();
        return results != null ? results : new ArrayList<Flight>();
    }


    public List<Flight> arrivingInBon(Long dAirportId, Date arrival) {
        Date bod = DateUtils.goToFirstHour(arrival);
        Date eod = DateUtils.goToEndOfTheDay(arrival);

        Query query = entityManager.createQuery("SELECT f " +
                "FROM Flight f " +
                "WHERE f.destinationAirport.id = :dAirport " +
                "AND f.departure > :bod AND f.departure < :eod");

        query.setParameter("dAirport", dAirportId).setParameter("bod", bod).setParameter("eod", eod);

        List<Flight> results = query.getResultList();
        return results != null ? results : new ArrayList<Flight>();
    }

}