package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 27/May/2014
 */
@Entity
@Table(name = "AIRPORTS")
public class Airport extends AbstractEntity {


    @Column(name = "code")
    private String code;

    @Column(name = "full_name")
    private String name;

    @Column(name = "latitude")
    private Float latitude;

    @Column(name = "longitude")
    private Float longitude;



    public Airport() {}

    public Airport(String code, String name, Float latitude, Float longitude) {
        this.code = code;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Airport) {
            Airport airport = (Airport) obj;
            return this.getId().equals(airport.getId());
        }
        return false;
    }
}
