package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.rest.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.generator.FlightsGenerator;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 10/09/15.
 */
@Component
@Path("/test")
@Consumes("application/json")
@Produces("application/json")
public class TestResource {

    @Autowired
    private FlightsGenerator flightsGenerator;


    @Path("/gen")
    @POST
    public Response generateFlights() {
        flightsGenerator.generateFlights();
        return Response.ok().build();
    }

}
