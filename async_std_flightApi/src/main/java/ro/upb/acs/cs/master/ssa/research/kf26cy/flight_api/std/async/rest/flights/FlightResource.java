package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.rest.flights;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.FlightResult;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.SearchFlightRequest;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.impl.FlightService;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.rest.util.ResponseUtils;

import javax.ws.rs.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Component
@Path("/flights")
@Consumes("application/json")
@Produces("application/json")
public class FlightResource {

    private static final long ASYNC_RESPONSE_TIMEOUT = 60000l;

    @Autowired
    private FlightService flightService;


    @GET
    public void searchFlights(@Suspend(ASYNC_RESPONSE_TIMEOUT) final AsynchronousResponse asyncResponse,
                              @QueryParam("start") Long start,
                              @QueryParam("end") Long end,

                              @QueryParam("source") Long sourceAirportId,
                              @QueryParam("destination") Long destinationAirportId,

                              @QueryParam("min") @DefaultValue("0") BigDecimal minPrice,
                              @QueryParam("max") BigDecimal maxPrice,

                              @QueryParam("oneway") @DefaultValue("true") boolean oneWay,
                              @QueryParam("direct") @DefaultValue("false") boolean directFlight) {


        Runnable worker = () -> {
            SearchFlightRequest request = SearchFlightRequest.SearchFlightRequestBuilder.aSearchFlightRequest()
                    .withStart(start)
                    .withEnd(end)
                    .withSourceAirportId(sourceAirportId)
                    .withDestinationAirportId(destinationAirportId)
                    .withMinPrice(minPrice)
                    .withMaxPrice(maxPrice)
                    .withOneWay(oneWay)
                    .withDirectFlight(directFlight)
                    .build();

            List<FlightResult> results = flightService.processSearchRequest(request);
            asyncResponse.setResponse(ResponseUtils.createSuccessfulResponse(results));
        };
        new Thread(worker).start();
    }

}
