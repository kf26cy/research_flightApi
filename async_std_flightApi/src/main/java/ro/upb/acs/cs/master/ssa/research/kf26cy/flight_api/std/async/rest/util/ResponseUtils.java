package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.rest.util;


import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
public class ResponseUtils {

    public static final String DATA_KEY = "data";


    public static Response createSuccessfulResponse() {
        return Response.ok().build();
    }


    public static <T> Response createSuccessfulResponse(T data) {
        Map<String, T> resultMap = new HashMap<String, T>();

        resultMap.put(DATA_KEY, data);
        return Response.ok(resultMap).build();
    }



}
