package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.std.async.service.person;

import retrofit.http.GET;

/**
 * Created by ktaDinca on 9/8/2015.
 */
public interface RandomApiService {

    @GET("/api")
    RandomApiResponse getRandomUser();

}
