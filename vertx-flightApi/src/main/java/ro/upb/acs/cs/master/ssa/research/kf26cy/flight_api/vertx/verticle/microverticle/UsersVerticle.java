package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.verticle.microverticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.rest.util.ResponseUtils;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.person.UserServiceAsyncHttpImpl;


/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 13-May-16.
 */
public class UsersVerticle extends AbstractVerticle {

    private static final Logger LOG = Logger.getLogger(UsersVerticle.class);
    private UserServiceAsyncHttpImpl userServiceAsync;

    public UsersVerticle(final ApplicationContext context) {
        this.userServiceAsync = (UserServiceAsyncHttpImpl) context.getBean("userServiceAsync");
    }

    public void handleGet(RoutingContext routingContext) {
        userServiceAsync.getUser(vertx, asyncResult -> {
            routingContext
                    .response()
                    .end(Json.encodePrettily(ResponseUtils.createSuccessfulResponseBody(asyncResult.result())));
        });
    }
}
