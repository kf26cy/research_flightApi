package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.verticle.microverticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import org.springframework.context.ApplicationContext;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.dao.AirportDao;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.rest.util.ResponseUtils;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.SearchFlightRequest;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.impl.FlightService;

import java.math.BigDecimal;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 24-Jan-16.
 */
public class FlightsVerticle extends AbstractVerticle {

    private AirportDao airportDao;
    private FlightService service;


    public FlightsVerticle(final ApplicationContext context) {
        service = (FlightService) context.getBean("flightService");
        airportDao = (AirportDao) context.getBean("airportDao");
    }


    @Override
    public void stop() throws Exception {

    }


    public void handleGet(RoutingContext routingContext) {
        SearchFlightRequest searchRequest = extractSearchRequest(routingContext.request());

        service.processSearchRequest(searchRequest, asyncResult -> {
            routingContext
                    .response()
                    .end(Json.encodePrettily(ResponseUtils.createSuccessfulResponseBody(asyncResult.result())));
        });
    }


    public void handletGetAirports(RoutingContext routingContext) {
        airportDao.loadAll(asyncResult -> {
            routingContext
                    .response()
                    .end(Json.encodePrettily(ResponseUtils.createSuccessfulResponseBody(asyncResult.result())));
        });
    }


    private static SearchFlightRequest extractSearchRequest(HttpServerRequest request) {
        String _start = request.getParam("start");
        String _end = request.getParam("end");

        String _sourceId = request.getParam("source");
        String _destinationId = request.getParam("destination");

        String _min = request.getParam("min");
        String _max = request.getParam("max");

        String _isOneWay = request.getParam("oneway");
        String _isDirect = request.getParam("direct");

        return SearchFlightRequest.SearchFlightRequestBuilder.aSearchFlightRequest()
                .withStart(_start != null ? Long.valueOf(_start): null)
                .withEnd(_end != null ? Long.valueOf(_end) : null)
                .withSourceAirportId(_sourceId != null ? Long.valueOf(_sourceId) : null)
                .withDestinationAirportId(_destinationId != null ? Long.valueOf(_destinationId) : null)
                .withMinPrice(_min != null ? new BigDecimal(_min) : new BigDecimal(0))
                .withMaxPrice(_max != null ? new BigDecimal(_max) : new BigDecimal(0))
                .withOneWay(_isOneWay != null ? Boolean.valueOf(_isOneWay) : true)
                .withDirectFlight(_isDirect != null ? Boolean.valueOf(_isDirect) : true)
                .build();
    }


}
