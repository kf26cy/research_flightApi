package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.dao;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model.Airport;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Repository
public class AirportDao extends AsyncBaseDao<Airport> {


    @Autowired
    public AirportDao(Vertx vertx, JsonObject config) {
        super(vertx, config);
    }


    /**
     * Async implementation.
     * <p>
     * <b>Beware!</b> MySQL hardcoded query
     * @param asyncResultHandler - handler/callback to be called when data arrives.
     */
    public void loadAll(Handler<AsyncResult<List<Airport>>> asyncResultHandler) {
        client.getConnection(connectionAsyncResult -> {
            SQLConnection connection = connectionAsyncResult.result();
            connection.query("SELECT * FROM AIRPORTS ORDER BY CODE", select -> {
                ResultSet resultSet = select.result();
                List<Airport> airports = resultSet.getRows()
                        .stream()
                        .map(Airport::new)
                        .collect(Collectors.toList());

                asyncResultHandler.handle(Future.succeededFuture(airports));

                // Explicitly close the connection after the handler is updated
                connection.close();
            });
        });
    }


    /**
     * Async implementation.
     * <p>
     * <b>Beware!</b> MySQL hardcoded query
     * @param id - airport id
     * @param asyncResultHandler - handler/callback to be called upon data arrival
     */
    public void findAirportById(Long id, Handler<AsyncResult<Airport>> asyncResultHandler) {

        client.getConnection(connectionAsyncResult -> {
            SQLConnection connection = connectionAsyncResult.result();
            connection.queryWithParams("SELECT * FROM AIRPORTS WHERE ID = ?", new JsonArray().add(id), select -> {
                ResultSet resultSet = select.result();
                Airport $airport = resultSet.getRows()
                        .stream()
                        .map(Airport::new)
                        .findFirst()
                        .get();

                asyncResultHandler.handle(Future.succeededFuture($airport));
                connection.close(closeHandler -> {
//                    if (closeHandler.succeeded()) {
//                        LOG.debug("Database Connection closed");
//                    }
//                    else if (closeHandler.failed()) {
//                        LOG.error("Database Connection failed to close!");
//                    }
                });
            });
        });
    }

}
