package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx;

import io.vertx.core.Vertx;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.verticle.ServerVerticle;

/**
 * Runner for the vert-x implementation
 */
public class ApplicationRunner {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");

        /*
         * Accessed via the single instance
         * final Vertx vertx = Vertx.vertx();
         */

        /*
         * Accessed via the Spring IoC container.
         */
        final Vertx vertx = (Vertx) context.getBean("vertx");

        vertx.deployVerticle(new ServerVerticle(context));
    }
}
