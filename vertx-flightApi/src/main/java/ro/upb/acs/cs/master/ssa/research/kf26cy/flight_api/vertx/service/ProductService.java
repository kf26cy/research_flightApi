package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 20-Jan-16.
 */
@Service
public class ProductService {

    public List<Product> getProducts() {
        return Arrays.asList(new Product("p1", 100L),
                new Product("p2", 200L),
                new Product("p3", 300L),
                new Product("p4", 400L)
        );
    }

    public static class Product {

        private String name;
        private Long price;

        public Product() {
        }

        public Product(String name, Long price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getPrice() {
            return price;
        }

        public void setPrice(Long price) {
            this.price = price;
        }
    }

}
