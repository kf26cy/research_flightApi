package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.json.JsonObject;

import java.io.IOException;

/**
 * @Entity
 * @Table(name = "AIRPORTS")
 *
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 27/May/2014
 */
public class Airport extends AbstractEntity {

    @JsonProperty("code")
    private String code;

    @JsonProperty("full_name")
    private String name;

    @JsonProperty("latitude")
    private Float latitude;

    @JsonProperty("longitude")
    private Float longitude;


    public Airport() {}

    public Airport(Long id) {
        this.setId(id);
    }

    public Airport(String code, String name, Float latitude, Float longitude) {
        this.code = code;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }



    /**
     * Cea mai maxima tiganie pe care am facut-o vreodata.
     */
    public Airport(JsonObject entries) {
        try {
            Airport $airport = (new ObjectMapper()).readValue(entries.toString(), Airport.class);
            this.setId($airport.getId());
            this.code = $airport.code;
            this.name = $airport.name;
            this.latitude = $airport.latitude;
            this.longitude = $airport.longitude;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Airport) {
            Airport airport = (Airport) obj;
            return this.getId().equals(airport.getId());
        }
        return false;
    }


}
