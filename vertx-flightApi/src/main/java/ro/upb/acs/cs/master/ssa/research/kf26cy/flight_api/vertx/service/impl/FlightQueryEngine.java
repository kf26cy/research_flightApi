package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model.Flight;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.Trip;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.dao.FlightDao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class FlightQueryEngine {

    @Autowired
    private FlightDao flightDao;


    public void findDirectOneWayTrips(Long sourceAirportId,
                                      Long destinationAirportId,
                                      Date start,
                                      Handler<AsyncResult<List<Trip>>> asyncResultHandler) {

        flightDao.betweenABon(sourceAirportId, destinationAirportId, start, asyncResult -> {
            List<Flight> flights = asyncResult.result();
            asyncResultHandler.handle(Future.succeededFuture(toTrips(flights)));
        });
    }


    public void findAllOneWayTrips(Long sourceAirportId,
                                   Long destinationAirportId,
                                   Date start,
                                   Handler<AsyncResult<List<Trip>>> callback) {
        /*
         * Build 2 futures that will eventually hold the direct and indirect flights.
         * The same is done when constructing the indirect flights, merging leaving
         * and arriving sources.
         */
        Future<List<Trip>> _directFlights = Future.future();
        Future<List<Trip>> _indirectFlights = Future.future();

        CompositeFuture.all(_directFlights, _indirectFlights).setHandler(allFlightsComputedHandler -> {
            if (allFlightsComputedHandler.succeeded()) {
                List<Trip> flightPlans = _directFlights.result();
                flightPlans.addAll(_indirectFlights.result());

                callback.handle(Future.succeededFuture(flightPlans));
            }
        });

        /*
         * Build 2 futures of List<Flight> that will complete when the leavingFrom and arrivingAt flights are ready.
         * These 2 are composed into another future (_indirectFlights).
         */
        Future<List<Flight>> _leavingFlights = Future.future();
        Future<List<Flight>> _arrivingFlights = Future.future();

        CompositeFuture.all(_leavingFlights, _arrivingFlights).setHandler(indirectFlightsAsyncResult -> {
            if (indirectFlightsAsyncResult.succeeded()) {
                _indirectFlights.complete(toTrips(_leavingFlights.result(), _arrivingFlights.result()));
            }
        });

        flightDao.leavingAon(sourceAirportId, start, asyncResultLeaving -> {
            _leavingFlights.complete(asyncResultLeaving.result());
        });
        flightDao.arrivingInBon(destinationAirportId, start, asyncResultArriving -> {
            _arrivingFlights.complete(asyncResultArriving.result());
        });

        findDirectOneWayTrips(sourceAirportId, destinationAirportId, start, directFlightsAsyncResult -> {
            _directFlights.complete(directFlightsAsyncResult.result());
        });
    }


    private static List<Trip> toTrips(List<Flight> step1Flights) {
        return step1Flights.stream()
                .map(Trip::new)
                .collect(Collectors.toList());
    }

    /**
     * TODO: refactor using streams.
     * @param leavingFrom
     * @param arrivingAt
     * @return
     */
    private List<Trip> toTrips(List<Flight> leavingFrom, List<Flight> arrivingAt) {
        List<Trip> trips = new ArrayList<Trip>();

        for (Flight leaving : leavingFrom) {
            for (Flight arriving : arrivingAt) {
                if (!leaving.getDestinationAirport().equals(arriving.getSourceAirport())) {
                    continue;
                }
                trips.add(new Trip(leaving, arriving));
            }
        }
        return trips;
    }
}