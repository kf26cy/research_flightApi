package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.dao;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model.Flight;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.util.DateUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Repository
public class FlightDao extends AsyncBaseDao<Flight> {

    private static final Logger LOG = Logger.getLogger(FlightDao.class);
    private Vertx vertx;

    @Autowired
    public FlightDao(Vertx vertx, JsonObject config) {
        super(vertx, config);
        this.vertx = vertx;
    }


    /**
     * TODO: since getConnection is a blocking operation, it must be run from a
     * worker verticle, or on an {@code Vertx#executeBlocking} block.
     */
    public void betweenABon(Long sAirportId, Long dAirportId, Date departure, Handler<AsyncResult<List<Flight>>> asyncResult) {
        client.getConnection(connectionAsyncResult -> {
            SQLConnection connection = connectionAsyncResult.result();

            Date bod = DateUtils.goToFirstHour(departure);
            Date eod = DateUtils.goToEndOfTheDay(departure);

            String query = "SELECT * FROM FLIGHTS " +
                            "WHERE SOURCE_AIRPORT_ID = ? " +
                            "AND DEST_AIRPORT_ID = ? " +
                            "AND DEPARTURE > ? AND DEPARTURE < ?";

            connection.queryWithParams(query, new JsonArray().add(sAirportId).add(dAirportId).add(bod.toInstant()).add(eod.toInstant()), select -> {
                ResultSet resultSet = select.result();

                List<Flight> flights = resultSet.getRows()
                        .stream()
                        .map(Flight::new)
                        .collect(Collectors.toList());

                asyncResult.handle(Future.succeededFuture(flights));
                connection.close();
            });
        });
    }


    public void leavingAon(Long sAirportId, Date departure, Handler<AsyncResult<List<Flight>>> asyncResult) {

        client.getConnection(connectionAsyncResult -> {
            SQLConnection connection = connectionAsyncResult.result();

            Date bod = DateUtils.goToFirstHour(departure);
            Date eod = DateUtils.goToEndOfTheDay(departure);

            String query = "SELECT * FROM FLIGHTS " +
                    "WHERE SOURCE_AIRPORT_ID = ? " +
                    "AND DEPARTURE > ? AND DEPARTURE < ?";

            connection.queryWithParams(query, new JsonArray().add(sAirportId).add(bod.toInstant()).add(eod.toInstant()), select -> {

                ResultSet resultSet = select.result();

                List<Flight> flights = resultSet.getRows()
                        .stream()
                        .map(Flight::new)
                        .collect(Collectors.toList());

                asyncResult.handle(Future.succeededFuture(flights));
                connection.close();
            });
        });
    }


    public void arrivingInBon(Long dAirportId, Date arrival, Handler<AsyncResult<List<Flight>>> asyncResult) {

        client.getConnection(connectionAsyncResult -> {
            SQLConnection connection = connectionAsyncResult.result();

            Date bod = DateUtils.goToFirstHour(arrival);
            Date eod = DateUtils.goToEndOfTheDay(arrival);

            String query = "SELECT * FROM FLIGHTS " +
                            "WHERE DEST_AIRPORT_ID = ? " +
                            "AND DEPARTURE > ? AND DEPARTURE < ?";

            connection.queryWithParams(query, new JsonArray().add(dAirportId).add(bod.toInstant()).add(eod.toInstant()), select -> {
                ResultSet resultSet = select.result();

                List<Flight> flights = resultSet.getRows()
                        .stream()
                        .map(Flight::new)
                        .collect(Collectors.toList());

                asyncResult.handle(Future.succeededFuture(flights));
                connection.close();
            });
        });
    }

}