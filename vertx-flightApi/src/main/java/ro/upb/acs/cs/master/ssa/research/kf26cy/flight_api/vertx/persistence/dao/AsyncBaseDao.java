package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.dao;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model.AbstractEntity;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 16-May-16.
 */
@Repository
public abstract class AsyncBaseDao<T extends AbstractEntity> {

    private static final Logger LOG = Logger.getLogger(AsyncBaseDao.class);
    protected JDBCClient client;


    @Autowired
    public AsyncBaseDao(Vertx vertx, @Qualifier("hikari-datasource-config") JsonObject config) {
        this.client = JDBCClient.createShared(vertx, config);
    }

    /**
     * TODO: only if necessary
     * No use-case for save in this implementation.
     */
//    public abstract void saveOrUpdate(T entity);

}
