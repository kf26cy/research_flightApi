package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.FlightResult;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.Trip;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.SearchFlightRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 09/Jun/2014
 */
@Service
public class FlightService {

    @Autowired
    private FlightQueryEngine flightQueryEngine;


    public void processSearchRequest(SearchFlightRequest request,
                                     Handler<AsyncResult<List<FlightResult>>> callback) {

        int isOneWay = request.isOneWay() ? 1 : -1;
        int isDirect = request.isDirectFlight() ? 2 : 4;
        int flightConstraints = isOneWay * isDirect;

        switch (flightConstraints) {
            /* one way direct flights */
            case 2:
                getDirectOneWayFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart(), directOneWayAsyncResult -> {
                    callback.handle(Future.succeededFuture(directOneWayAsyncResult.result()));
                });
                break;

            /* one way indirect flight */
            case 4:
                getIndirectOneWayFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart(), indirectOneWayAsyncResult -> {
                    callback.handle(Future.succeededFuture(indirectOneWayAsyncResult.result()));
                });
                break;

            /* round trip direct flight*/
            case -2:
                getDirectRoundTripFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart(), request.getEnd(), directRoundTripAsyncResult -> {
                    callback.handle(Future.succeededFuture(directRoundTripAsyncResult.result()));
                });
                break;

            /* round trip indirect flight*/
            case -4:
                getIndirectRoundTripFlights(request.getSourceAirportId(), request.getDestinationAirportId(), request.getStart(), request.getEnd(), indirectRoundTripAsyncResult -> {
                    callback.handle(Future.succeededFuture(indirectRoundTripAsyncResult.result()));
                });
                break;
        }
    }


    private void getIndirectRoundTripFlights(Long sourceAirportId,
                                             Long destinationAirportId,
                                             Date start,
                                             Date end,
                                             Handler<AsyncResult<List<FlightResult>>> callback) {

        /*
         * The two lists of flights are computed asynchronously and their data will be
         * available in 2 futures. The futures are merged, and the callback is called
         * when the data is processed.
         */
        Future<List<Trip>> forwardFuture = Future.future();
        Future<List<Trip>> backFuture = Future.future();

        CompositeFuture.all(forwardFuture, backFuture).setHandler(bothCompleteFuture -> {
            callback.handle(Future.succeededFuture(buildResults(forwardFuture.result(), backFuture.result())));
        });

        flightQueryEngine.findAllOneWayTrips(sourceAirportId, destinationAirportId, start, forwardAsyncResult -> {
            forwardFuture.complete(forwardAsyncResult.result());
        });

        flightQueryEngine.findAllOneWayTrips(destinationAirportId, sourceAirportId, end, backAsyncResult -> {
            backFuture.complete(backAsyncResult.result());
        });
    }


    private void getDirectRoundTripFlights(Long sourceAirportId,
                                                         Long destinationAirportId,
                                                         Date start,
                                                         Date end,
                                                         Handler<AsyncResult<List<FlightResult>>> callback) {

        /*
         * The two lists of flights are computed asynchronously and their data will be
         * available in 2 futures. The futures are merged, and the callback is called
         * when the data is processed.
         */
        Future<List<Trip>> forwardFuture = Future.future();
        Future<List<Trip>> backFuture = Future.future();

        CompositeFuture.all(forwardFuture, backFuture).setHandler(bothCompleteFuture -> {
            if (bothCompleteFuture.succeeded()) {
                callback.handle(Future.succeededFuture(buildResults(forwardFuture.result(), backFuture.result())));
            }
        });

        flightQueryEngine.findDirectOneWayTrips(sourceAirportId, destinationAirportId, start, forwardAsyncResult -> {
            forwardFuture.complete(forwardAsyncResult.result());
        });

        flightQueryEngine.findDirectOneWayTrips(destinationAirportId, sourceAirportId, end, backAsyncResult -> {
            backFuture.complete(backAsyncResult.result());
        });
    }


    private void getIndirectOneWayFlights(Long sourceAirportId,
                                          Long destinationAirportId,
                                          Date start, Handler<AsyncResult<List<FlightResult>>> callback) {

        flightQueryEngine.findAllOneWayTrips(sourceAirportId, destinationAirportId, start, tripsAsyncResult -> {
            List<Trip> indirectOneWayFlights = tripsAsyncResult.result();
            callback.handle(Future.succeededFuture(buildResults(indirectOneWayFlights)));
        });
    }


    private void getDirectOneWayFlights(Long sourceAirportId,
                                        Long destinationAirportId,
                                        Date start,
                                        Handler<AsyncResult<List<FlightResult>>> callback) {

        flightQueryEngine.findDirectOneWayTrips(sourceAirportId, destinationAirportId, start, tripsAsyncResult -> {
            List<Trip> directOneWayFlights = tripsAsyncResult.result();
            callback.handle(Future.succeededFuture(buildResults(directOneWayFlights)));
        });
    }


    private List<FlightResult> buildResults(List<Trip> oneWayTrips) {
        if (oneWayTrips == null) {
            return new ArrayList<FlightResult>();
        }

        return oneWayTrips
                .parallelStream()
                .map(FlightResult::new)
                .collect(Collectors.toList());
    }


    /**
     * Makes a cartesian product from the 2 lists.
     * TODO: refactor using jdk 8 streams
     */
    private List<FlightResult> buildResults(List<Trip> forward, List<Trip> back) {
        if (forward == null || back == null) {
            return new ArrayList<FlightResult>();
        }
        ArrayList<FlightResult> results = new ArrayList<FlightResult>();
        for (Trip _forward : forward) {
            for (Trip _back : back) {
                results.add(new FlightResult(_forward, _back));
            }
        }
        return results;
    }

}
