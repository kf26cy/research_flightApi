package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.rest.util;


import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 12/09/15.
 */
public class ResponseUtils {

    private static final String DATA_KEY = "data";


    public static <T> Map<String, T> createSuccessfulResponseBody(T data) {
        Map<String, T> resultMap = new HashMap<String, T>();

        resultMap.put(DATA_KEY, data);
        return resultMap;
    }

}
