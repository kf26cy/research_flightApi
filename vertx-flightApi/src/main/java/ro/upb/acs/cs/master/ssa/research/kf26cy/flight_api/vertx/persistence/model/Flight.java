package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.json.JsonObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Entity
 * @Table(name = "FLIGHTS")
 *
 * @author Catalin Dinca (alexandru.dinca2110@gmail.com)
 * @since 25/May/2014
 */
public class Flight extends AbstractEntity {

    @JsonProperty("departure")
    private Date departure;

    @JsonProperty("arrival")
    private Date dueArrival;

    @JsonProperty("source_airport_id")
    private Airport sourceAirport;

    @JsonProperty("dest_airport_id")
    private Airport destinationAirport;

    @JsonProperty("price")
    private BigDecimal price;

    @JsonProperty("company")
    private String company;


    public Flight() {}

    public Flight(Date departure, Date dueArrival, Airport sourceAirport, Airport destinationAirport, BigDecimal price, String company) {
        this.departure = departure;
        this.dueArrival = dueArrival;
        this.sourceAirport = sourceAirport;
        this.destinationAirport = destinationAirport;
        this.price = price;
        this.company = company;
    }

    /**
     * Awful hack.
     * TODO: please find a clean way of doing this shit.
     *
     * @param entries
     * @throws IOException
     */
    public Flight(JsonObject entries) {
        $FlightInnerModel _flight = null;
        try {
            _flight = (new ObjectMapper()).readValue(entries.toString(), $FlightInnerModel.class);
            this.setId(_flight.getId());
            this.departure = _flight.getDeparture();
            this.dueArrival = _flight.getDueArrival();
            this.sourceAirport = new Airport(_flight.getSourceAirport());
            this.destinationAirport = new Airport(_flight.getDestinationAirport());
            this.price = _flight.getPrice();
            this.company = _flight.getCompany();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getDueArrival() {
        return dueArrival;
    }

    public void setDueArrival(Date dueArrival) {
        this.dueArrival = dueArrival;
    }

    public Airport getSourceAirport() {
        return sourceAirport;
    }

    public void setSourceAirport(Airport sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public Airport getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(Airport destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    private static class $FlightInnerModel {

        @JsonProperty("id")
        private Long id;

        @JsonProperty("departure")
        private Date departure;

        @JsonProperty("arrival")
        private Date dueArrival;

        @JsonProperty("source_airport_id")
        private Long sourceAirport;

        @JsonProperty("dest_airport_id")
        private Long destinationAirport;

        @JsonProperty("price")
        private BigDecimal price;

        @JsonProperty("company")
        private String company;


        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Date getDeparture() {
            return departure;
        }

        public void setDeparture(Date departure) {
            this.departure = departure;
        }

        public Date getDueArrival() {
            return dueArrival;
        }

        public void setDueArrival(Date dueArrival) {
            this.dueArrival = dueArrival;
        }

        public Long getSourceAirport() {
            return sourceAirport;
        }

        public void setSourceAirport(Long sourceAirport) {
            this.sourceAirport = sourceAirport;
        }

        public Long getDestinationAirport() {
            return destinationAirport;
        }

        public void setDestinationAirport(Long destinationAirport) {
            this.destinationAirport = destinationAirport;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }
    }

}
