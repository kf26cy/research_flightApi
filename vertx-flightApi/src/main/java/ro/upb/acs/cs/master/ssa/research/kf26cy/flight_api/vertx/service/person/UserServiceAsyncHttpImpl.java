package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.person;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author Alexandru Dinca (alexandru.dinca2110@gmail.com)
 * @since 13-May-16.
 */
@Service("userServiceAsync")
public class UserServiceAsyncHttpImpl {


    public void getUser(Vertx vertx, Handler<AsyncResult<RandomApiResponse>> asyncResultHandler) {
        HttpClientOptions options = new HttpClientOptions()
                .setDefaultHost("randomuser.me")
                .setSsl(true)
                .setDefaultPort(443)
                .setTrustAll(true);
        HttpClient httpClient = vertx.createHttpClient(options);

        /*
         * The Handler implementation passed to the getNow() method is called when the
         * headers of the HTTP response are received. If you do not need to access the
         * response body, you can process the response already in this handler.
         * However, if you do need to access the body of the HTTP response, you need to
         * register another handler on the HttpClientResponse that is passed as parameter
         * to the first Handler's handle() method
         */
        httpClient.getNow("/api", response -> {

            /*
             * The Handler implementation passed to the bodyHandler() method of the
             * HttpClientResponse is called when the full HTTP response body is received.
             */
            response.bodyHandler(buffer -> {
                ObjectMapper mapper = new ObjectMapper();
                try { asyncResultHandler.handle(Future.succeededFuture(mapper.readValue(buffer.getBytes(), RandomApiResponse.class))); }
                catch (IOException e) { e.printStackTrace(); asyncResultHandler.handle(Future.failedFuture(e)); }
            });
        });
    }
}
