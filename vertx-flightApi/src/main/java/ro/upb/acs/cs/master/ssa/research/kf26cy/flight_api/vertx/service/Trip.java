package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service;

import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.persistence.model.Flight;

import java.math.BigDecimal;


public class Trip {

    private Flight step1;
    private Flight step2;
    private BigDecimal price;

    public Trip(Flight step1) {
        this(step1, null);
    }

    public Trip(Flight step1, Flight step2) {
        this.step1 = step1;
        this.step2 = step2;
        this.price = computePrice(step1.getPrice(), step2 != null ? step2.getPrice() : new BigDecimal(0));
    }

    private BigDecimal computePrice(BigDecimal price1, BigDecimal price2) {
        return price1.add(price2);
    }

    public Flight getStep1() {
        return step1;
    }

    public Flight getStep2() {
        return step2;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
