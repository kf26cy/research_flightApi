package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.service.person;

import java.util.List;

/**
 * Created by ktaDinca on 9/8/2015.
 */
public class RandomApiResponse {

    public List<Result> results;
    public Info info;

    public List<Result> 	getResults		() 						{ return results; }
    public void 			setResults		(List<Result> results) 	{ this.results = results; }
    public Info             getInfo         ()                      { return info; }
    public void             setInfo         (Info info)             { this.info = info; }

    public static class Info {
        private String seed;
        private Integer results;
        private Integer page;
        private String version;

        public String getSeed() {
            return seed;
        }

        public void setSeed(String seed) {
            this.seed = seed;
        }

        public Integer getResults() {
            return results;
        }

        public void setResults(Integer results) {
            this.results = results;
        }

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }


    public static class Result {
        private String gender;
        private Name name;
        private Location location;
        private String email;
        private Login login;
        private String registered;
        private String dob;
        private String phone;
        private ID id;
        private String cell;
        private String DNI;
        private Picture picture;
        private String version;
        private String nat;

        public String      getGender       ()                  { return gender; }
        public void        setGender       (String gender)     { this.gender = gender; }
        public Name        getName         ()                  { return name; }
        public void        setName         (Name name)         { this.name = name; }
        public Location    getLocation     ()                  { return location; }
        public void        setLocation     (Location location) { this.location = location; }
        public String      getEmail        ()                  { return email; }
        public void        setEmail        (String email)      { this.email = email; }
        public String      getRegistered   ()                  { return registered; }
        public void        setRegistered   (String registered) { this.registered = registered; }
        public String      getDob          ()                  { return dob; }
        public void        setDob          (String dob)        { this.dob = dob; }
        public String      getPhone        ()                  { return phone; }
        public void        setPhone        (String phone)      { this.phone = phone; }
        public String      getCell         ()                  { return cell; }
        public void        setCell         (String cell)       { this.cell = cell; }
        public String      getDNI          ()                  { return DNI; }
        public void        setDNI          (String DNI)        { this.DNI = DNI; }
        public Picture     getPicture      ()                  { return picture; }
        public void        setPicture      (Picture picture)   { this.picture = picture; }
        public String      getVersion      ()                  { return version; }
        public void        setVersion      (String version)    { this.version = version; }
        public Login       getLogin        ()                  { return login; }
        public void        setLogin        (Login login)       { this.login = login; }
        public ID          getId           ()                  { return id; }
        public void        setId           (ID id)             { this.id = id; }
        public String      getNat          ()                  { return nat; }
        public void        setNat          (String nat)        { this.nat = nat; }

        private static class Location {
            private String street;
            private String city;
            private String state;
            private String postcode;

            public String getStreet     ()                  { return street; }
            public void setStreet       (String street)     { this.street = street;}
            public String getCity       ()                  { return city; }
            public void setCity         (String city)       { this.city = city; }
            public String getState      ()                  { return state; }
            public void setState        (String state)      { this.state = state; }
            public String getPostcode   ()                  { return postcode;}
            public void setPostcode     (String postcode)   { this.postcode = postcode;}
        }

        public static class Login {
            private String username;
            private String password;
            private String salt;
            private String md5;
            private String sha1;
            private String sha256;

            public String getUsername() { return username; }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getSalt() {
                return salt;
            }

            public void setSalt(String salt) {
                this.salt = salt;
            }

            public String getMd5() {
                return md5;
            }

            public void setMd5(String md5) {
                this.md5 = md5;
            }

            public String getSha1() {
                return sha1;
            }

            public void setSha1(String sha1) {
                this.sha1 = sha1;
            }

            public String getSha256() {
                return sha256;
            }

            public void setSha256(String sha256) {
                this.sha256 = sha256;
            }
        }

        public static class ID {
            private String name;
            private String value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        private static class Name {
            private String title;
            private String first;
            private String last;

            public String getTitle      ()                  { return title; }
            public void setTitle        (String title)      { this.title = title; }
            public String getFirst      ()                  { return first; }
            public void setFirst        (String first)      { this.first = first; }
            public String getLast       ()                  { return last; }
            public void setLast         (String last)       { this.last = last; }
        }

        private static class Picture {
            private String large;
            private String medium;
            private String thumbnail;

            public String getLarge      ()                  { return large; }
            public void setLarge        (String large)      { this.large = large; }
            public String getMedium     ()                  { return medium; }
            public void setMedium(      String medium)      { this.medium = medium; }
            public String getThumbnail  ()                  { return thumbnail; }
            public void setThumbnail    (String thumbnail)  { this.thumbnail = thumbnail; }
        }
    }
}
