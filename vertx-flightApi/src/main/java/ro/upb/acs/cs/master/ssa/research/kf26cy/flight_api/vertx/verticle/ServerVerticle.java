package ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.verticle.microverticle.FlightsVerticle;
import ro.upb.acs.cs.master.ssa.research.kf26cy.flight_api.vertx.verticle.microverticle.UsersVerticle;


public class ServerVerticle extends AbstractVerticle {

    private static final Logger LOG = Logger.getLogger(ServerVerticle.class);
    private ApplicationContext context;


    public ServerVerticle(ApplicationContext context) {
        this.context = context;
    }


    @Override
    public void start() throws Exception {
        super.start();

        Router rootRouter = Router.router(vertx);
        Router mainRouter = Router.router(vertx);

        FlightsVerticle flightsVerticle = new FlightsVerticle(context);
        UsersVerticle usersVerticle = new UsersVerticle(context);

        vertx.deployVerticle(usersVerticle, new DeploymentOptions().setWorker(true), handler -> {
            if (handler.succeeded()) {
                mainRouter.mountSubRouter("/user", getUsersRouter(usersVerticle));
            }
        });

        vertx.deployVerticle(flightsVerticle, handler -> {
            if (handler.succeeded()) {
                mainRouter.mountSubRouter("/flights", getFlightsRouter(flightsVerticle));
            }
        });

        mainRouter.get("/").handler(routingContext -> {
            routingContext.response()
                    .putHeader("content-type", "text/html")
                    .end("<h1>Hello from my first Vert.x 3 application</h1>");
        });

        rootRouter.mountSubRouter("/flight-api/api/", mainRouter);

        HttpServer server = vertx.createHttpServer();
        server.requestHandler(rootRouter::accept);
        server.listen(8080);
    }

    private Router getFlightsRouter(final FlightsVerticle flightsVerticle) {
        Router flightsRouter = Router.router(vertx);
        flightsRouter.get("/")
                .produces("application/json")
                .handler(flightsVerticle::handleGet);

        flightsRouter.get("/airports")
                .produces("application/json")
                .handler(flightsVerticle::handletGetAirports);

        return flightsRouter;
    }


    public Router getUsersRouter(final UsersVerticle usersVerticle) {
        Router productsRouter = Router.router(vertx);
        productsRouter.get("/")
                .produces("application/json")
                .handler(usersVerticle::handleGet);
        return productsRouter;
    }


    public Router getProductsRouter() {
        Router productsRouter = Router.router(vertx);
        productsRouter.get("/")
                .produces("application/json")
                .handler(routingContext -> {
                    routingContext.response()
                            .putHeader("content-type", "text/html")
                            .end("<h1>Hello from my first Vert.x 3 application</h1>");
                });
        return productsRouter;
    }
}
